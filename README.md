# Trigger to Ansible Tower

An [example script](https://bitbucket.org/thaumos/project-bb/src/master/tower.bash)
for launching an Ansible Tower job from Bitbucket Pipelines.

## How to use it

* Add required environment variables to your Bitbucket environment variables.
* Make sure your already has an Ansible YAML file.
* Copy `tower.bash` to your project.
* Add `tower.bash` to your build configuration.


## Required environment variables

* **`host`**: Tower host
* **`username`**: Tower username
* **`password`**: Tower user password
* **`ID`**: ID of job template being launched.

## Trigger the Ansible job

```
hostval=$(tower-cli config host $host)
userval=$(tower-cli config username $username)
passwordval=$(tower-cli config password $password)
tower-cli config verify_ssl false
tower-cli job launch --job-template $ID --monitor
```

* Uses `-ex` switches (not shown in snippet above) to exit on first error and to echo command before execution. These help with debugging.
* Uses `tower-cli` to configure host, username, and password within the Docker instance.
* Uses `tower-cli` to make sure the connection is possible.
* Uses `tower-cli` to launch the job.

## Build configuration

```
image: python:2.7
pipelines:
  default:
    - step:
        script: # Modify the commands below to build your repository.
            - pip install ansible-tower-cli
            - ./tower.bash
```

* Uses the standard Python image, which already contains Pip.
* Installs the Tower CLI, which is already available from PyPI.
* Triggers the Ansible job.
